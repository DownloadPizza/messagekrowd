# MessageKrowd

MessageKrowd is a communication plugin providing core chat features. It automatically fetches the prefix and suffix via LuckPerms. It also integrates with BanManager to check if the player is muted.

With MessageKrowd, all chat messages become system messages, so it is not possible for players to use Mojang's chat reporting system. We use system messages mainly because they're easier to deal with (e.g. applying text formatting to a message).

## Features

* Ability to group servers into groups sharing chat
* Integrates with LuckPerms to fetch prefix/suffix
* Private messaging with /m, /msg, /w or /message
* Smart-reply with /r or /reply
* Away-from-keyboard functionality with /afk
* Custom styles for private messages
* Permission for use of chat colour codes
* Censor filter to prevent bad words
* Duplicate-message, caps-limit and rate-limit spam protection
* Socialspy for moderators with /socialspy
* Ability to silence per chatgroup or globally
* Optional: Checks if the player is muted in BanManager
* Optional: AFK support with BungeeTabListPlus
* Prevents the "Chat messages can't be verified" toast from appearing.

## Installation

Drop the plugin into the plugin folder on all servers including BungeeCord and restart.
Remember to edit the config on the bungeecord server. It is required to restart
for the config to update.

## Permissions

* messagekrowd.formatcodes - gives full access to use of chat formatting codes
* messagekrowd.formatcodeslimited - gives limited access to use of chat formatting codes (&m &n &o &r)
* messagekrowd.socialspy - gives access to /socialspy
* messagekrowd.admin - gives access to /messagekrowd utility/debug command
* messagekrowd.afk - gives access to go afk with the /afk command
* messagekrowd.silence - gives access to /silence
* messagekrowd.silencebypass - gives permission to bypass the silence restriction

## Credits

This plugin is Open-Source, released under AGPL-3.0.
If you want to contribute you can find the repository at:  
[https://gitlab.com/cubekrowd/messagekrowd](https://gitlab.com/cubekrowd/messagekrowd "https://gitlab.com/cubekrowd/messagekrowd")
