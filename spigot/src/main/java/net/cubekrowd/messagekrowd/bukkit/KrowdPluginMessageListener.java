package net.cubekrowd.messagekrowd.bukkit;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.UUID;
import java.util.logging.Level;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;

public class KrowdPluginMessageListener implements PluginMessageListener {

    private final MessageKrowdBukkitPlugin plugin;

    public KrowdPluginMessageListener(MessageKrowdBukkitPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public void onPluginMessageReceived(String channel, Player player, byte[] message) {
        DataInputStream in = new DataInputStream(new ByteArrayInputStream(message));
        String subChannel = null;
        try {
            subChannel = in.readUTF();

            if (subChannel.equals("ForwardChat")) {
                var uuid = new UUID(in.readLong(), in.readLong());
                var theMessage = in.readUTF();
                var messageSender = Bukkit.getPlayer(uuid);
                if (messageSender == null) {
                    plugin.getLogger().warning("Received message from offline player: " + uuid + ", message: " + theMessage);
                } else {
                    try {
                        messageSender.chat(theMessage);
                    } catch (Exception e) {
                        plugin.getLogger().log(Level.WARNING, "Failed to handle message", e);
                    }
                }
            } else if (subChannel.equals("ConsoleMessage")) {
                var theMessage = in.readUTF();
                var console = plugin.getServer().getConsoleSender();
                console.sendMessage(theMessage);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
