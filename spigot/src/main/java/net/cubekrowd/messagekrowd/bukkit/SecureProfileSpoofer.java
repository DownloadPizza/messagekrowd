package net.cubekrowd.messagekrowd.bukkit;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOutboundHandlerAdapter;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.ChannelPromise;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import net.minecraft.network.protocol.game.ClientboundServerDataPacket;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.network.ServerLoginPacketListenerImpl;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_19_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.plugin.Plugin;

public class SecureProfileSpoofer implements Listener {
    public static final String SUPPORTED_VERSION = "1.19.3";
    public static final String HANDLER_NAME = "messagekrowd-secure-profile-spoofer";

    public final Plugin plugin;
    public boolean enabled;

    public SecureProfileSpoofer(Plugin plugin) {
        this.plugin = plugin;
    }

    public static ChannelPipeline findLoginPlayerPipeline(Player p) {
        var handle = ((CraftPlayer) p).getHandle();
        for (var connection : MinecraftServer.getServer().getConnection().getConnections()) {
            if (connection.getPacketListener() instanceof ServerLoginPacketListenerImpl loginPacketListener) {
                if (handle.gameProfile == loginPacketListener.gameProfile) {
                    return connection.channel.pipeline();
                }
            }
        }
        return null;
    }

    public static ChannelPipeline getGamePipeline(Player p) {
        var handle = ((CraftPlayer) p).getHandle();
        return handle.connection.getConnection().channel.pipeline();
    }

    public void tryEnable() {
        if (enabled) {
            return;
        }

        if (Bukkit.getMinecraftVersion().equals(SUPPORTED_VERSION)) {
            enabled = true;
            Bukkit.getPluginManager().registerEvents(this, plugin);
            for (var online : Bukkit.getOnlinePlayers()) {
                try {
                    attach(online, getGamePipeline(online));
                } catch (Exception e) {
                    plugin.getLogger().log(Level.SEVERE, String.format("Failed to attach to %s", online.getName()), e);
                }
            }
        } else {
            plugin.getLogger().warning(String.format("Secure profile spoofing is not supported: need MC %s, got %s", SUPPORTED_VERSION, Bukkit.getMinecraftVersion()));
        }
    }

    public void disable() {
        if (!enabled) {
            return;
        }

        enabled = false;
        HandlerList.unregisterAll(this);
        for (var online : Bukkit.getOnlinePlayers()) {
            try {
                detach(online, getGamePipeline(online));
            } catch (Exception e) {
                plugin.getLogger().log(Level.SEVERE, String.format("Failed to detach from %s", online.getName()), e);
            }
        }
    }

    public void attach(Player p, ChannelPipeline pipeline) {
        // NOTE(traks): ensure we don't register the handler multiple times!
        detach(p, pipeline);
        if (pipeline.get("packet_handler") == null) {
            plugin.getLogger().warning(String.format("No packet handler found for %s", p.getName()));
        } else {
            pipeline.addBefore("packet_handler", HANDLER_NAME, new PacketHandler());
        }
    }

    public void detach(Player p, ChannelPipeline pipeline) {
        try {
            pipeline.remove(HANDLER_NAME);
        } catch (NoSuchElementException e) {
            // NOTE(traks): ignore
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onLogin(PlayerLoginEvent e) {
        // NOTE(traks): The packet listener object in the connection field isn't
        // available yet, so we need to search for it
        var pipeline = findLoginPlayerPipeline(e.getPlayer());
        if (pipeline == null) {
            plugin.getLogger().severe(String.format("No pipeline found for %s", e.getPlayer().getName()));
        } else {
            attach(e.getPlayer(), pipeline);
        }
    }

    // NOTE(traks): theoretically the server data only gets sent once when the
    // player joins, so we don't need this packet handler anymore afterwards.
    // Keep it around anyway in case some plugin decides to be obnoxious
    public static class PacketHandler extends ChannelOutboundHandlerAdapter {
        @Override
        public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
            if (msg instanceof ClientboundServerDataPacket packet) {
                // NOTE(traks): set enforce secure chat to true always, so
                // players don't see the "Chat messages can't be verified" toast
                // whenever they join (or bungee'd switch servers).
                //
                // Note that currently (1.19.3), offline mode servers (i.e.
                // bungee'd servers) always have enforce secure chat disabled
                // server-side. Only the bungee config option controls whether
                // players get kicked.
                var spoofedPacket = new ClientboundServerDataPacket(
                        packet.getMotd().orElse(null),
                        packet.getIconBase64().orElse(null),
                        true
                );
                msg = spoofedPacket;
            }
            super.write(ctx, msg, promise);
        }
    }
}
