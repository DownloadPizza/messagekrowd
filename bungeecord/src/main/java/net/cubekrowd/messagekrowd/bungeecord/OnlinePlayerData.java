package net.cubekrowd.messagekrowd.bungeecord;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class OnlinePlayerData {
    // accumulated points for rate limited messages & commands
    public AtomicInteger rateLimitPoints = new AtomicInteger();
    // last time since we detected the player to not be AFK. -1 if currently AFK
    public AtomicLong lastSeenMillis = new AtomicLong();
    public ConcurrentHashMap<String, String> displayNameByServer = new ConcurrentHashMap<>();

    // NOTE(traks): only modified in compute functions of the
    // UUID -> online data hashmap
    public int onlineCount;

    public void markSeen(MessageKrowdBungeeCordPlugin plugin, ProxiedPlayer pp) {
        var prevLastSeen = lastSeenMillis.getAndSet(System.currentTimeMillis());

        if (prevLastSeen == -1) {
            // @TODO(traks) currently this could happen:
            //
            //   1. thread A sets lastSeenMillis to -1 making player AFK
            //   2. thread B updates lastSeenMillis and sees player is AFK
            //   3. thread B sends unAFK message
            //   4. thread A sends AFK message
            //
            // Now players think player is AFK even though they aren't. No
            // real way around this AFAIK without some better
            // synchronisation. But I don't really want to send AFK message
            // while holding some lock, because the AFK message sending also
            // dumps stuff in ESAPI, which is slooooow
            plugin.printAFK(pp, false);
        }
    }

    public boolean isAFK() {
        return lastSeenMillis.get() == -1;
    }

    public static boolean shouldBeAFK(long lastSeenMillis, long curTimeMillis, long afkTime) {
        var inactiveMillis = curTimeMillis - lastSeenMillis;
        return inactiveMillis / 1000 > afkTime;
    }

    public void markAFKIfLongNotSeen(MessageKrowdBungeeCordPlugin plugin, ProxiedPlayer pp, long afkTime) {
        long curTimeMillis = System.currentTimeMillis();
        var prevLastSeenMillis = lastSeenMillis.getAndUpdate(x -> {
            if (x == -1 || shouldBeAFK(x, curTimeMillis, afkTime)) {
                return -1;
            } else {
                return x;
            }
        });

        if (prevLastSeenMillis == -1) {
            // already AFK
            return;
        }

        if (shouldBeAFK(prevLastSeenMillis, curTimeMillis, afkTime)) {
            // @TODO(traks) see AFK issue above
            plugin.printAFK(pp, true);
        }
    }

    public void toggleAFK(MessageKrowdBungeeCordPlugin plugin, ProxiedPlayer pp) {
        var curTimeMillis = System.currentTimeMillis();
        var newLastSeenMillis = lastSeenMillis.updateAndGet(x -> {
            if (x == -1) {
                // make non-afk
                return curTimeMillis;
            } else {
                // make afk
                return -1;
            }
        });

        // @TODO(traks) see AFK issue above
        boolean nowAFK = (newLastSeenMillis == -1);
        plugin.printAFK(pp, nowAFK);
    }

    public String getServerDisplayName(ProxiedPlayer pp) {
        var serverName = pp.getServer().getInfo().getName();
        var res = displayNameByServer.get(serverName);
        if (res == null) {
            // fall back to generic display name if we haven't received a custom
            // display name from the server
            res = pp.getDisplayName();
        }
        return res;
    }
}
