package net.cubekrowd.messagekrowd.bungeecord;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import net.luckperms.api.LuckPermsProvider;

public class LuckPermsHook {
    public static List<String> getTrackNames(MessageKrowdBungeeCordPlugin plugin) {
        var res = new ArrayList<String>();
        var trackManager = LuckPermsProvider.get().getTrackManager();
        try {
            trackManager.loadAllTracks().get();
        } catch (InterruptedException | ExecutionException e) {
            plugin.getLogger().log(Level.SEVERE, "Failed to retrieve LuckPerms tracks", e);
            return res;
        }

        for (var track : trackManager.getLoadedTracks()) {
            res.add(track.getName());
        }
        return res;
    }
}
