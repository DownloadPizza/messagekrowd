package net.cubekrowd.messagekrowd.bungeecord;

import codecrafter47.bungeetablistplus.api.bungee.Variable;
import java.util.Objects;
import net.luckperms.api.LuckPermsProvider;
import net.luckperms.api.node.NodeType;
import net.luckperms.api.node.types.InheritanceNode;
import net.md_5.bungee.api.connection.ProxiedPlayer;

/**
 * BTLP variable returning the weight of the highest rank in the given track
 * belonging to a player.
 */
public class TrackWeightVariable extends Variable {

    // Track to search
    private final String trackName;

    public TrackWeightVariable(String track) {
        super("messagekrowd_trackweight_" + track);
        this.trackName = track;
    }

    @Override
    public String getReplacement(ProxiedPlayer pp) {
        var api = LuckPermsProvider.get();
        var track = api.getTrackManager().getTrack(trackName);
        if (track == null) {
            api.getTrackManager().loadTrack(trackName);
            return "0";
        }

        var user = api.getUserManager().getUser(pp.getUniqueId());
        if (user == null) {
            api.getUserManager().loadUser(pp.getUniqueId());
            return "0";
        }

        // Get max weight in the given track
        var weight = user.getNodes().stream()
            .filter(NodeType.INHERITANCE::matches)
            .map(node -> ((InheritanceNode) node).getGroupName())
            .filter(track::containsGroup)
            .map(api.getGroupManager()::getGroup)
            .filter(Objects::nonNull)
            .mapToInt(group -> group.getWeight().orElse(0))
            .max()
            .orElse(0);

        // we can instruct BTLP to properly sort numbers
        // (not lexicographically) by writing something like
        // "messagekrowd_trackweight_donator as number asc"
        return Integer.toString(weight);
    }
}
