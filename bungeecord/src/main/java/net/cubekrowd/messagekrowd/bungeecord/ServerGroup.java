package net.cubekrowd.messagekrowd.bungeecord;

import java.util.ArrayList;
import java.util.List;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;

public class ServerGroup {
    public final String name;
    public final List<String> serverNames = new ArrayList<>();

    public ServerGroup(String name) {
        this.name = name;
    }

    public List<ServerInfo> getLiveServers() {
        var res = new ArrayList<ServerInfo>();
        for (var serverName : serverNames) {
            var si = ProxyServer.getInstance().getServerInfo(serverName);
            if (si != null) {
                res.add(si);
            }
        }
        return res;
    }
}
