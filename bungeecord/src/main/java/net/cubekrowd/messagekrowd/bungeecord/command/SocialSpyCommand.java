package net.cubekrowd.messagekrowd.bungeecord.command;

import net.cubekrowd.messagekrowd.bungeecord.MessageKrowdBungeeCordPlugin;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class SocialSpyCommand extends Command {

    private final MessageKrowdBungeeCordPlugin plugin;

    public SocialSpyCommand(MessageKrowdBungeeCordPlugin plugin) {
        super("socialspy", "messagekrowd.socialspy", "spy");
        this.plugin = plugin;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            sender.sendMessage(plugin.prefix + ChatColor.RED +
                               "This is a player-only command");
            return;
        }
        var pp = (ProxiedPlayer) sender;
        var uuid = pp.getUniqueId();

        var newSpying = plugin.allSocialSpies.compute(uuid, (k, v) -> {
            if (v == null) {
                // add if wasn't in the list yet
                return (byte) 0;
            } else {
                // remove if already in list
                return null;
            }
        });

        if (newSpying == null) {
            // no longer spying
            sender.sendMessage(plugin.prefix + ChatColor.DARK_AQUA
                    + "Turned socialspy " + ChatColor.RED + "off");
        } else {
            // now spying
            sender.sendMessage(plugin.prefix + ChatColor.DARK_AQUA
                    + "Turned socialspy " + ChatColor.GREEN + "on");
        }

        plugin.saveSocialSpies();
    }
}
