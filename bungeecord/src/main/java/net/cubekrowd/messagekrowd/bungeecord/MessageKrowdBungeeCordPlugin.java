package net.cubekrowd.messagekrowd.bungeecord;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import net.cubekrowd.eventstorageapi.api.EventEntry;
import net.cubekrowd.eventstorageapi.api.EventStorageAPI;
import net.cubekrowd.messagekrowd.bungeecord.command.AFKCommand;
import net.cubekrowd.messagekrowd.bungeecord.command.ClearChatCommand;
import net.cubekrowd.messagekrowd.bungeecord.command.MessageCommand;
import net.cubekrowd.messagekrowd.bungeecord.command.MessageKrowdCommand;
import net.cubekrowd.messagekrowd.bungeecord.command.ReplyCommand;
import net.cubekrowd.messagekrowd.bungeecord.command.SilenceCommand;
import net.cubekrowd.messagekrowd.bungeecord.command.SocialSpyCommand;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

public class MessageKrowdBungeeCordPlugin extends Plugin {
    private static final String MSG_CHANNEL = "messagekrowd:generic";

    public boolean hasLuckPerms;
    public boolean hasESAPI;
    public boolean hasBTLP;

    // @NOTE(traks) hold global lock when accessing any of these fields
    public final Map<UUID, UUID> replyMap = new HashMap<>();
    public final Set<String> silencedGroups = new HashSet<>();
    public final Object globalLock = new Object();

    // @NOTE(traks) we use this as a hash set, we don't care about the value.
    // Avoid boolean as value because that'll lead to confusing null pointers
    // when e.g. calling remove(key): returns the value instead of whether key
    // was removed. Byte seems most sensible alternative, because it occupies
    // least amount of memory.
    public final ConcurrentHashMap<UUID, Byte> allSocialSpies = new ConcurrentHashMap<>();
    public final Object socialSpiesSaveLock = new Object();

    // NOTE(traks): Big problem is that if player has joined, and then joins on
    // another connection, it kicks the first connection and fires the
    // disconnect event of the first connection after the postlogin of the
    // second! This makes plugins think the player is online twice or has
    // disconnected even if they are still online :(
    //
    // Unlikely to ever get fixed as this has already been the case for years.
    // See https://github.com/SpigotMC/BungeeCord/issues/2371 and linked issues.
    //
    // In the past we got around this by not actually creating online data on
    // join, but only creating the data when requested. However, this seems to
    // cause issues with e.g. players AFKing immediately on join, probably
    // because their online data didn't get removed properly. I'm guessing
    // something is recreating the player data after the player has officially
    // disconnected?
    //
    // Now we keep track of how many times the player is online and only remove
    // the online data when the player is 0 times online.
    public ConcurrentHashMap<UUID, OnlinePlayerData> onlineDataMap = new ConcurrentHashMap<>();

    public final String prefix = ChatColor.DARK_GRAY + "[" + ChatColor.DARK_AQUA +
                                 "Message" + ChatColor.GOLD + "Krowd" + ChatColor.DARK_GRAY + "] ";

    // @NOTE(traks) when you need the settings, assign this field to a local
    // variable and use that local variable to access the settings.
    public volatile MessageKrowdSettings globalSettings;

    @Override
    public void onEnable() {
        // integrate with other plugins

        if (getProxy().getPluginManager().getPlugin("LuckPerms") == null) {
            getLogger().warning("LuckPerms not detected. Skipping instant prefix update support and multi-track sorting.");
        } else {
            getLogger().info("LuckPerms detected. Using it for instant prefix update support and multi-track sorting.");
            hasLuckPerms = true;
        }

        if (getProxy().getPluginManager().getPlugin("BungeeTabListPlus") == null) {
            getLogger().warning("BungeeTabListPlus not detected. Skipping tablist AFK support and multi-track sorting.");
        } else {
            getLogger().info("BungeeTabListPlus detected. Using it for tablist AFK support and multi-track sorting.");
            hasBTLP = true;
        }

        if (getProxy().getPluginManager().getPlugin("EventStorageAPI") == null) {
            getLogger().warning("EventStorageAPI not detected. Skipping logging chat and commands.");
        } else {
            getLogger().info("EventStorageAPI detected. Using it for logging chat and commands.");
            hasESAPI = true;
        }

        if (hasBTLP) {
            BungeeTabListPlusHook.registerAFKVariable(this);

            // get all tracks from LuckPerms and register variables for them
            if (hasLuckPerms) {
                for (var trackName : LuckPermsHook.getTrackNames(this)) {
                    getLogger().info("Registering track weight variable for track " + trackName);
                    BungeeTabListPlusHook.registerTrackVariable(this, trackName);
                }
            }
        }

        // load config

        saveDefaultConfig("config.yml");
        if (!reloadFromConfig()) {
            // @TODO(traks) special handling?
            return;
        }

        // load data

        var data = loadConfig("data.yml");
        for (var spy : data.getStringList("socialspy")) {
            allSocialSpies.put(UUID.fromString(spy), (byte) 0);
        }

        // set up commands and stuff

        getProxy().registerChannel(MSG_CHANNEL);
        getProxy().getPluginManager().registerListener(this, new KrowdListener(this));
        getProxy().getPluginManager().registerListener(this, new ChatListener(this));

        getProxy().getPluginManager().registerCommand(this, new MessageCommand(this));
        getProxy().getPluginManager().registerCommand(this, new ReplyCommand(this));
        getProxy().getPluginManager().registerCommand(this, new SocialSpyCommand(this));
        getProxy().getPluginManager().registerCommand(this,
                new MessageKrowdCommand(this));
        getProxy().getPluginManager().registerCommand(this, new AFKCommand(this));
        getProxy().getPluginManager().registerCommand(this, new SilenceCommand(this));
        getProxy().getPluginManager().registerCommand(this, new ClearChatCommand(this));

        getProxy().getScheduler().schedule(this, () -> {
            var settings = globalSettings;

            for (var entry : onlineDataMap.entrySet()) {
                var uuid = entry.getKey();
                var pOnlineData = entry.getValue();
                var pp = ProxyServer.getInstance().getPlayer(uuid);

                pOnlineData.rateLimitPoints.updateAndGet(x -> x <= 0 ? 0 : (x - 1));

                if (pp != null) {
                    pOnlineData.markAFKIfLongNotSeen(this, pp, settings.afkTime);
                }
            }
        }, 1, 1, TimeUnit.SECONDS);
    }

    @Override
    public void onDisable() {
        // @NOTE(traks) don't bother cleaning things up, because BungeeCord
        // doesn't support reloading anyway and we don't use any reloading
        // plugins on BungeeCord
    }

    public void saveDefaultConfig(String name) {
        getDataFolder().mkdirs();
        var configFile = new File(getDataFolder(), name);
        try {
            Files.copy(getResourceAsStream(name), configFile.toPath());
        } catch (FileAlreadyExistsException e) {
            // ignore
        } catch (IOException e) {
            getLogger().log(Level.WARNING, "Can't save default config " + name, e);
        }
    }

    public Configuration loadConfig(String name) {
        var configProvider = ConfigurationProvider.getProvider(YamlConfiguration.class);
        var file = new File(getDataFolder(), name);
        try {
            return configProvider.load(file);
        } catch (IOException e) {
            if (file.exists()) {
                getLogger().log(Level.WARNING, "Can't load config " + name, e);
            }
            return new Configuration();
        }
    }

    public boolean reloadFromConfig() {
        var settings = new MessageKrowdSettings();
        if (!settings.load(this, loadConfig("config.yml"))) {
            return false;
        }

        synchronized (globalLock) {
            globalSettings = settings;
            silencedGroups.clear();
        }
        return true;
    }

    public void saveSocialSpies() {
        // @NOTE(traks) social spy toggling happens so little, doing the dumbest
        // thing possible should be good enough.
        //
        // Keep lock while getting the social spy list and saving because: this
        // ensures that the saved list is eventually consistent with the
        // in-memory data, and because writing to the same file from different
        // threads may corrupt it.
        synchronized (socialSpiesSaveLock) {
            var data = new Configuration();
            var stringSpies = new ArrayList<String>();
            allSocialSpies.forEach((k, v) -> stringSpies.add(k.toString()));
            data.set("socialspy", stringSpies);

            try {
                ConfigurationProvider.getProvider(YamlConfiguration.class).save(
                        data, new File(getDataFolder(), "data.yml"));
            } catch (IOException e) {
                getLogger().log(Level.SEVERE, "Failed to save social spies", e);
            }
        }
    }

    public OnlinePlayerData getOnlineData(ProxiedPlayer pp) {
        var uuid = pp.getUniqueId();
        return onlineDataMap.get(uuid);
    }

    public boolean isAFK(ProxiedPlayer pp) {
        var onlineData = getOnlineData(pp);
        if (onlineData == null) {
            return false;
        }
        return onlineData.isAFK();
    }

    public ProxiedPlayer matchOnlinePlayer(String match) {
        // Replicates CraftBukkit's Server#getPlayer implementation, so it
        // behaves as players expect it to behave.
        var res = getProxy().getPlayer(match);
        if (res != null) {
            return res;
        }

        var lowerMatch = match.toLowerCase(Locale.ENGLISH);
        int resNameLen = Integer.MAX_VALUE;

        for (ProxiedPlayer online : getProxy().getPlayers()) {
            var lowerName = online.getName().toLowerCase(Locale.ENGLISH);
            if (!lowerName.startsWith(lowerMatch)) {
                continue;
            }
            if (lowerName.length() < resNameLen) {
                res = online;
                resNameLen = lowerName.length();
                if (resNameLen == 0) {
                    // won't get any better than this
                    break;
                }
            }
        }
        return res;
    }

    public void forwardMessageToBukkit(ProxiedPlayer pp, String message) {
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("ForwardChat");
        var uuid = pp.getUniqueId();
        out.writeLong(uuid.getMostSignificantBits());
        out.writeLong(uuid.getLeastSignificantBits());
        out.writeUTF(message);
        pp.getServer().sendData(MSG_CHANNEL, out.toByteArray());
    }

    public void logMessageToBukkitConsole(ServerInfo server, String message) {
        if (server.getPlayers().isEmpty()) {
            // TODO(traks): we kind of need to do this. Otherwise, if a server
            // hasn't been joined for a while and a player suddenly joins, they
            // will get kicked for packet spam (all the chat messages pile up).
            // This will also result in console spam. We should probably move
            // away from using plugin messages for this at some point. The whole
            // problem is that plugin messages can't be sent to servers with no
            // players online.
            return;
        }
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("ConsoleMessage");
        out.writeUTF(message);
        server.sendData(MSG_CHANNEL, out.toByteArray());
    }

    public String getMessageChannelName() {
        return MSG_CHANNEL;
    }

    public void printAFK(ProxiedPlayer pp, boolean isNowAfk) {
        if (hasESAPI) {
            Map<String, String> data = new HashMap<>();
            data.put("u", pp.getUniqueId().toString());
            data.put("s", "" + isNowAfk);
            EventStorageAPI.getStorage().addEntry(new EventEntry(getDescription().getName(), "afk", data));
        }

        var settings = globalSettings;
        String message = isNowAfk ? settings.afkGoneFormat : settings.afkBackFormat;
        message = message.replaceAll("\\{sender\\}", pp.getName());
        message = ChatColor.translateAlternateColorCodes('&', message);

        var messagef = message;
        var msgComponent = TextComponent.fromLegacyText(messagef);
        var group = settings.getServerGroupByServer(pp.getServer().getInfo());
        for (var si : group.getLiveServers()) {
            si.getPlayers().forEach(sp -> sp.sendMessage(msgComponent));
            logMessageToBukkitConsole(si, messagef);
        }
    }
}
