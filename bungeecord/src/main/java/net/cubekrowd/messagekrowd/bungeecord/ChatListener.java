package net.cubekrowd.messagekrowd.bungeecord;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import net.cubekrowd.eventstorageapi.api.EventEntry;
import net.cubekrowd.eventstorageapi.api.EventStorageAPI;
import net.luckperms.api.LuckPermsProvider;
import net.luckperms.api.query.QueryOptions;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.TranslatableComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.event.EventPriority;

public class ChatListener implements Listener {
    private final MessageKrowdBungeeCordPlugin plugin;

    public ChatListener(MessageKrowdBungeeCordPlugin plugin) {
        this.plugin = plugin;
    }

    // TODO(traks): MC 1.19 client turns non-breaking spaces into spaces and
    // trims spaces from beginning and end, before sending the player input
    // text to the server. Currently we just convert all multi-spaces into a
    // single space, because Paper does so too. We might want to make it so
    // multi-spaces stay.
    public static String respace(String theMessage) {
        var sb = new StringBuilder();
        var cpIter = theMessage.codePoints().iterator();
        boolean spaceStarted = false;
        while (cpIter.hasNext()) {
            var cp = cpIter.nextInt();
            if (Character.isSpaceChar(cp)) {
                if (!spaceStarted) {
                    sb.append(' ');
                    spaceStarted = true;
                }
            } else {
                spaceStarted = false;
                sb.appendCodePoint(cp);
            }
        }
        return sb.toString().trim();
    }

    public void executeCommand(ProxiedPlayer pp, String message) {
        if (!ProxyServer.getInstance().getPluginManager().dispatchCommand(pp, message.substring(1))) {
            plugin.forwardMessageToBukkit(pp, message);
        }
    }

    public String getDisplayName(ProxiedPlayer pp, OnlinePlayerData onlineData, MessageKrowdSettings settings) {
        var display = onlineData.getServerDisplayName(pp);
        if (AprilFools.isAprilFools()) display = AprilFools.reverse(display);
        if (plugin.hasLuckPerms && !settings.teamPrefixSuffixServers.contains(pp.getServer().getInfo().getName())) {
            var api = LuckPermsProvider.get();
            var user = api.getUserManager().getUser(pp.getUniqueId());
            if (user != null) {
                var context = api.getContextManager().getContext(user);
                if (context.isPresent()) {
                    var metaData = user.getCachedData().getMetaData(QueryOptions.contextual(context.get()));
                    if (AprilFools.isAprilFools()) display = AprilFools.reverse(pp.getDisplayName());
                    display = (metaData.getPrefix() != null ? metaData.getPrefix() : "") +
                            display + (metaData.getSuffix() != null ? metaData.getPrefix() : "");
                    display = ChatColor.translateAlternateColorCodes('&', display);
                }
            }
        }
        return display;
    }

    public String filterMessage(ProxiedPlayer pp, String theMessage, OnlinePlayerData onlineData, MessageKrowdSettings settings) {
        // TODO(traks): Chars vs unicode codepoints may require some special
        // care below

        var ppServer = pp.getServer().getInfo();
        var ppServerGroup = settings.getServerGroupByServer(ppServer);
        var ppServerName = ppServer.getName();

        // Replace fullwidth characters and other characters commonly used
        // for spam and bypassing swear filter
        if (settings.replaceFullwidth) {
            char[] charMessage = theMessage.toCharArray();
            for (int i = 0; i < charMessage.length; i++) {
                if (charMessage[i] >= '\uFF00' && charMessage[i] < '\uFF60') {
                    // Fullwidth characters
                    charMessage[i] -= '\uFF00' - ' ';
                } else if (charMessage[i] >= '\u249C' && charMessage[i] < '\u24B6') {
                    // Enclosed alphanumerics, letters of the form (x)
                    charMessage[i] -= '\u249C' - 'a';
                } else if (charMessage[i] >= '\u24B6' && charMessage[i] < '\u24D0') {
                    // Enclosed alphanumerics, circled capital letters
                    charMessage[i] -= '\u24B6' - 'A';
                } else if (charMessage[i] >= '\u24D0' && charMessage[i] < '\u24EA') {
                    // Enclosed alphanumerics, circled lowercase letters
                    charMessage[i] -= '\u24D0' - 'a';
                }
            }
            theMessage = new String(charMessage);
        }

        //
        // SPAM Protection
        //
        // Check caps-limit
        String workm = theMessage;
        // remove all usernames
        for (var onlinePlayer : plugin.getProxy().getPlayers()) {
            workm = workm.replaceAll(onlinePlayer.getName().toUpperCase(Locale.ENGLISH), "");
        }
        // @NOTE(traks) Ignore spaces in messages for caps filtering, so
        // messages with lots of caps but with small words (which are common)
        // still get filtered.
        workm = workm.replaceAll(" ","");

        boolean oneCapsWord = true;
        int capsWord = 0;
        for (String word : theMessage.split(" ")) {
            if (word.equals(word.toUpperCase(Locale.ENGLISH))) {
                capsWord++;
            }
        }
        if (capsWord > 1) oneCapsWord = false;

        var spamProtectedServers = settings.spamProtectedServers;
        if (workm.length() > 3 && (spamProtectedServers.size() == 0
                || spamProtectedServers.contains(ppServerName))) {
            double uppercase = workm.chars().mapToObj(i -> (char) i).filter(Character::isUpperCase).count();
            if ((100 * (uppercase / workm.length()) >= settings.capsLimit && !oneCapsWord) || uppercase > 16) {
                pp.sendMessage(ChatColor.translateAlternateColorCodes('&', settings.capsMessage));
                theMessage = theMessage.toLowerCase(Locale.ENGLISH);
            }
        }

        // Character spam
        int longestChain = 0;
        int cChain = 1;
        for (int i = 1; i < workm.length(); i++) {
            if (workm.charAt(i) == workm.charAt(i - 1)) {
                cChain++;
            } else {
                if (cChain > longestChain) longestChain = cChain;
                cChain = 1;
            }
        }
        if (cChain > longestChain) longestChain = cChain;

        if (workm.length() > 3 && (spamProtectedServers.size() == 0
                || spamProtectedServers.contains(ppServerName))) {
            if (longestChain > settings.charLimit) {
                pp.sendMessage(ChatColor.translateAlternateColorCodes('&', settings.charMessage));
                return null;
            }
        }

        // Rate-limit
        var newPoints = onlineData.rateLimitPoints.addAndGet(settings.rateLimitPointsPerMessage);
        if (newPoints > settings.rateLimitThreshold && (spamProtectedServers.isEmpty()
                || spamProtectedServers.contains(ppServerName))) {
            pp.sendMessage(ChatColor.translateAlternateColorCodes('&', settings.rateLimitMessage));
            return null;
        }

        //
        // Censor filter
        //
        if (settings.censorEnabled && (settings.censoredServers.isEmpty()
                || settings.censoredServers.contains(ppServerName))) {
            for (String word : settings.censoredWords) {
                if (settings.censorBlock) {
                    if (theMessage.toLowerCase(Locale.ENGLISH).contains(word)) {
                        pp.sendMessage(ChatColor.translateAlternateColorCodes('&', settings.censorMessage));
                        return null;
                    }
                } else {
                    var stars = "*".repeat(word.length());
                    theMessage = theMessage.replaceAll("(?i)" + word, stars);
                }
            }
        }
        return theMessage;
    }

    public String formatMessage(ProxiedPlayer pp, String theMessage) {
        if (pp.hasPermission("messagekrowd.formatcodes")) {
            theMessage = ChatColor.translateAlternateColorCodes('&', theMessage).replaceAll(ChatColor.MAGIC + "", "");
        }
        if (pp.hasPermission("messagekrowd.formatcodeslimited")) {
            theMessage = theMessage.replaceAll("&m", "" + ChatColor.STRIKETHROUGH);
            theMessage = theMessage.replaceAll("&n", "" + ChatColor.UNDERLINE);
            theMessage = theMessage.replaceAll("&o", "" + ChatColor.ITALIC);
            theMessage = theMessage.replaceAll("&r", "" + ChatColor.RESET);
        }
        return theMessage;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onChat(ChatEvent e) {
        // Do not process cancelled events
        if (e.isCancelled()) {
            return;
        }
        if (!(e.getSender() instanceof ProxiedPlayer pp)) {
            return;
        }

        // NOTE(traks): BungeeCord 1.19 ignores any changes to the chat message
        // when it forwards it to the backend server, due to the new signed
        // message stuff. Just handle everything ourselves to avoid this
        // problem.
        e.setCancelled(true);

        var theMessage = e.getMessage();

        {
            // @NOTE(traks) BungeeCord already does this, but only since
            // recently. Might as well keep it to stay on the safe side. Who
            // knows what BungeeCord decides to change...
            for (int i = 0; i < theMessage.length(); i++) {
                char c = theMessage.charAt(i);
                if (c == '§' || c < ' ' || c == 127) {
                    // @NOTE(traks) illegal character, disconnect
                    pp.disconnect(new TranslatableComponent("multiplayer.disconnect.illegal_characters"));
                    return;
                }
            }
        }

        var ppServer = pp.getServer().getInfo();
        var settings = plugin.globalSettings;
        var ppServerGroup = settings.getServerGroupByServer(ppServer);
        var ppServerName = ppServer.getName();
        var onlineData = plugin.getOnlineData(pp);

        if (onlineData == null) {
            plugin.getLogger().info(String.format(Locale.ENGLISH, "Got chat message from offline player %s", pp.getName()));
            return;
        }

        if (!theMessage.toLowerCase(Locale.ENGLISH).startsWith("/afk")) {
            onlineData.markSeen(plugin, pp);
        }

        theMessage = respace(theMessage);

        if (theMessage.startsWith("/")) {
            // Log commands
            if (plugin.hasESAPI) {
                Map<String, String> data = new HashMap<>();
                data.put("u", pp.getUniqueId().toString());
                data.put("s", pp.getServer().getInfo().getName());
                data.put("m", theMessage);
                EventStorageAPI.getStorage().addEntry(new EventEntry(plugin.getDescription().getName(), "command", data));
            }

            // Check list of commands to process (these should be chat or message-related)
            var processCommand = false;
            for (var cmd : settings.commandsToProcess) {
                var msg = theMessage.toLowerCase(Locale.ENGLISH);
                var prefix = "/" + cmd.toLowerCase(Locale.ENGLISH);
                if (msg.startsWith(prefix + " ")) {
                    processCommand = true;
                    break;
                }
            }

            if (processCommand) {
                theMessage = filterMessage(pp, theMessage, onlineData, settings);
            }
            if (theMessage == null) {
                return;
            }

            executeCommand(pp, theMessage);
            return;
        }

        // NOTE(traks): now handle chat messages

        var forwardToBukkit = settings.passthroughServers.contains(ppServerName);

        theMessage = filterMessage(pp, theMessage, onlineData, settings);
        if (theMessage == null) {
            return;
        }

        String display = getDisplayName(pp, onlineData, settings);

        // Use formatting codes in chat
        // Bukkit does not allow § in chat, so pass through unprocessed text
        // in that case.
        if (!forwardToBukkit) {
            theMessage = formatMessage(pp, theMessage);
        }

        long time = System.currentTimeMillis();

        synchronized (plugin.globalLock) {
            // @NOTE(traks) need to do all this stuff so messages for sure don't
            // appear after a chat group has been silenced (and the staff member
            // received the message confirming the silence)

            // Check if silence is in effect
            if (plugin.silencedGroups.contains(ppServerGroup.name)
                    && !pp.hasPermission("messagekrowd.silencebypass")) {
                pp.sendMessage(ChatColor.translateAlternateColorCodes('&', settings.silenceFormat));
                return;
            }

            if (ppServerGroup.serverNames.size() > 1) {
                logChat(pp, "g_" + ppServerGroup.name, display, theMessage, time);

                // If this message is to be passed through to the bukkit server,
                // do not send it to all players or the messagekrowd bukkit plugin
                // (to avoid duplicate messages arriving in chat)
                if (forwardToBukkit) {
                    plugin.forwardMessageToBukkit(pp, theMessage);
                    return;
                }

                var fullMessage = display + " " + ChatColor.RESET + theMessage;
                var component = TextComponent.fromLegacyText(fullMessage);
                for (var si : ppServerGroup.getLiveServers()) {
                    si.getPlayers().forEach(sp -> sp.sendMessage(component));
                    plugin.logMessageToBukkitConsole(si, fullMessage);
                }
            } else {
                // search for a group did not return the function which means no result
                // add brackets because of no group to immitate vanilla behaviour
                display = "<" + display + ChatColor.RESET + ">";
                logChat(pp, "s_" + ppServer.getName(), display, theMessage, time);

                // If this message is to be passed through to the bukkit server,
                // do not send it to all players or the messagekrowd bukkit plugin
                // (to avoid duplicate messages arriving in chat)
                if (forwardToBukkit) {
                    plugin.forwardMessageToBukkit(pp, theMessage);
                    return;
                }

                var fullMessage = display + " " + theMessage;
                var component = TextComponent.fromLegacyText(fullMessage);
                ppServer.getPlayers().forEach(sp -> sp.sendMessage(component));
                plugin.logMessageToBukkitConsole(ppServer, fullMessage);
            }
        }
    }

    private void logChat(ProxiedPlayer pp, String target, String display, String message, long time) {
        if (plugin.hasESAPI) {
            Map<String, String> data = new HashMap<>();
            data.put("u", pp.getUniqueId().toString());
            data.put("t", target);
            data.put("d", display);
            data.put("m", message);
            EventStorageAPI.getStorage().addEntry(new EventEntry(plugin.getDescription().getName(), "chat", time, data));
        }
    }
}
